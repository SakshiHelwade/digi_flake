import React, { useEffect, useState } from "react";
import "../style/State.css";
import { FaEdit } from "react-icons/fa";
import { RiDeleteBin6Line } from "react-icons/ri";
import city from "../assets/images/skyline.png";

const City = () => {
  const [data, setData] = useState([]);
  const [showEditModal, setShowEditModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [showAddModal, setShowAddModal] = useState(false);
  const [selectedCity, setSelectedCity] = useState(null);
  const [newCityName, setNewCityName] = useState("");
  const [newCityCode, setNewCityCode] = useState("");
  const [newStatus, setNewStatus] = useState("");
  const [newStateName, setNewStateName] = useState("");
  const [newCity, setNewCity] = useState({
    CityName: "",
    CityCode: "",
    StateName: "",
    status: "",
  });
  const [searchTerm, setSearchTerm] = useState("");
  const [newState, setnewState] = useState("");

  const handleEdit = (city) => {
    setSelectedCity(city);
    setNewCityName(city.CityName);
    setNewCityCode(city.CityCode);
    setNewStatus(city.status);
    setNewStateName(city.StateName);
    setShowEditModal(true);
  };

  const handleDelete = (city) => {
    
    if(city.status === "Active"){
      const newData = {
        CityName: city.CityName,
        CityCode: city.CityCode,
        StateName: city.StateName,
        status: "Inactive"
      };
      const updatedData = data.map(item =>
        item.id === city.id ? { ...item, ...newData } : item
      );
      setData(updatedData);
    }else if(city.status === 'Inactive'){
      setSelectedCity(city);
      setShowDeleteModal(true);
    }
  };

  const handleAdd = () => {
    setShowAddModal(true);
  };

  const handleAddSave = () => {
    const payload = {
      id: Date.now(),
      ...newCity,
    };
    setData([...data, payload]);
    setShowAddModal(false);
    setNewCity({
      CityName: "",
      CityCode: "",
      StateName: "",
      status: "",
    });
  };

  const handleEditSave = () => {
    const newData = {
      CityName: newCityName,
      CityCode: newCityCode,
      StateName: newStateName,
      status: newStatus,
    };
    const updatedData = data.map((item) =>
      item.id === selectedCity.id ? { ...item, ...newData } : item
    );
    setData(updatedData);
    setShowEditModal(false);
    setSelectedCity(null);
  };

  const handleDeleteConfirm = () => {
    const updatedData = data.filter((item) => item.id !== selectedCity.id);
    setData(updatedData);
    setShowDeleteModal(false);
    setSelectedCity(null);
  };

  // Filter data based on search term
  const filteredData = data.filter(
    (city) =>
      city.CityName.toLowerCase().includes(searchTerm.toLowerCase()) ||
      city.CityCode.toLowerCase().includes(searchTerm.toLowerCase()) ||
      city.StateName.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <div className="container">
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            width: "30px",
            height: "40px",
          }}
        >
          <img src={city} alt="" />
        </div>
        <div style={{ width: "80%" }}>
          <input
            type="text"
            placeholder="Search by Name, State, or City"
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
          />
        </div>

        <div>
          <button
            type="button"
            class="btn btn-primary"
            data-toggle="modal"
            data-target="#exampleModal"
            onClick={handleAdd}
            style={{
              color: "white",
              backgroundColor: "purple",
              padding: "10px",
              borderRadius:'30px',
              width:'130px'
            }}
          >
            Add New
          </button>
        </div>
      </div>

      <table className="table">
        <thead>
          <tr>
            <th>City Name</th>
            <th>City Code</th>
            <th>State Name</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {filteredData.map((city) => (
            <tr key={city.id}>
              <td>{city.CityName}</td>
              <td>{city.CityCode}</td>
              <td>{city.StateName}</td>
              <td
                className={
                  city.status === "Active" ? "status-active" : "status-inactive"
                }
              >
                {city.status}
              </td>
              <td style={{ display: "flex", gap: "10px" }}>
                <button onClick={() => handleEdit(city)}>
                  <FaEdit style={{ color: "gray" }} />
                </button>
                <button onClick={() => handleDelete(city)}>
                  <RiDeleteBin6Line style={{ color: "gray" }} />
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      {showAddModal && (
        <div className="modal-container show">
          <div className="modal">
            <span className="close" onClick={() => setShowAddModal(false)}>
              &times;
            </span>
            <h2>Add City</h2>
            <label>City Name:</label>
            <input
              type="text"
              value={newCity.CityName}
              onChange={(e) =>
                setNewCity({ ...newCity, CityName: e.target.value })
              }
            />
            <label>City Code:</label>
            <input
              type="text"
              value={newCity.CityCode}
              onChange={(e) =>
                setNewCity({ ...newCity, CityCode: e.target.value })
              }
            />
            <label>State:</label>
            <input
              type="text"
              value={newCity.StateName}
              onChange={(e) =>
                setNewCity({ ...newCity, StateName: e.target.value })
              }
            />
            <label>Status:</label>
            <input
              type="text"
              value={newCity.status}
              onChange={(e) =>
                setNewCity({ ...newCity, status: e.target.value })
              }
            />
            <button onClick={handleAddSave}>Save</button>
          </div>
        </div>
      )}

      {showEditModal && (
        <div className="modal-container show">
          <div className="modal">
            <span className="close" onClick={() => setShowEditModal(false)}>
              &times;
            </span>
            <h2>Edit City</h2>
            <label>City Name:</label>
            <input
              type="text"
              value={newCityName}
              onChange={(e) => setNewCityName(e.target.value)}
            />
            <label>City Code:</label>
            <input
              type="text"
              value={newCityCode}
              onChange={(e) => setNewCityCode(e.target.value)}
            />

            <label>State:</label>
            <input
              type="text"
              value={newStateName}
              onChange={(e) => setNewStateName(e.target.value)}
            />
            <label>Status:</label>
            <input
              type="text"
              value={newStatus}
              onChange={(e) => setNewStatus(e.target.value)}
            />
            <button onClick={handleEditSave}>Save Changes</button>
          </div>
        </div>
      )}

      {showDeleteModal && (
        <div className="modal-container show">
          <div className="modal">
            <span className="close" onClick={() => setShowDeleteModal(false)}>
              &times;
            </span>
            <h2>Confirm Delete</h2>
            <p>Are you sure you want to delete this city?</p>
            <button onClick={handleDeleteConfirm}>Delete</button>
            <button onClick={() => setShowDeleteModal(false)}>Cancel</button>
          </div>
        </div>
      )}
    </div>
  );
};

export default City;
