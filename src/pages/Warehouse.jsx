import React, { useEffect, useState } from 'react';
import "../style/State.css";
import { FaEdit } from "react-icons/fa";
import { RiDeleteBin6Line } from "react-icons/ri";
import warehouse from "../assets/images/warehouse.png";

const Warehouse = () => {
  const [data, setData] = useState([]);
  const [showEditModal, setShowEditModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [selectedWarehouse, setSelectedWarehouse] = useState(null);
  const [newWarehouseName, setNewWarehouseName] = useState('');
  const [newStatus, setNewStatus] = useState('');
  const [newState, setNewState] = useState('');
  const [newCity, setNewCity] = useState('');
  const [showAddModal, setShowAddModal] = useState(false);
  const [newWarehouse, setNewWarehouse] = useState({
    name: '',
    status: '',
    state: '',
    city: ''
  });
  const [searchTerm, setSearchTerm] = useState('');


  const handleEdit = (warehouse) => {
    setSelectedWarehouse(warehouse);
    setNewWarehouseName(warehouse.name);
    setNewStatus(warehouse.status);
    setNewCity(warehouse.city);
    setNewState(warehouse.state);
    setShowEditModal(true);
  };

  const handleDelete = (warehouse) => {
    if(warehouse.status === "Active"){
      const newData = {
        name: warehouse.name,
        state: warehouse.state,
        city: warehouse.city,
        status: "Inactive"
      };
      const updatedData = data.map(item =>
        item.id === warehouse.id ? { ...item, ...newData } : item
      );
      setData(updatedData);
    }else if(warehouse.status === 'Inactive'){
      setSelectedWarehouse(warehouse);
      setShowDeleteModal(true);
    }
  };

  const handleAdd = () => {
    setShowAddModal(true);
  };

  const handleAddSave = () => {
    const payload = {
      id: Date.now(),
      ...newWarehouse
    };
    setData([...data, payload]);
    setShowAddModal(false);
    setNewWarehouse({
      name: '',
      status: '',
      state: '',
      city: ''
    });
  };

  const handleEditSave = () => {
    const newData = {
      name: newWarehouseName,
      status: newStatus,
      state: newState,
      city: newCity
    };
    const updatedData = data.map(item =>
      item.id === selectedWarehouse.id ? { ...item, ...newData } : item
    );
    setData(updatedData);
    setShowEditModal(false);
    setSelectedWarehouse(null);
  };

  const handleDeleteConfirm = () => {
    const updatedData = data.filter(item => item.id !== selectedWarehouse.id);
    setData(updatedData);
    setShowDeleteModal(false);
    setSelectedWarehouse(null);
  };

  // Filter data based on search term
  const filteredData = data.filter(warehouse =>
    warehouse.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
    warehouse.state.toLowerCase().includes(searchTerm.toLowerCase()) ||
    warehouse.city.toLowerCase().includes(searchTerm.toLowerCase())
  );

  const [selectedState, setSelectedState] = useState("");
  const [selectedCity, setSelectedCity] = useState("");
  

  const handleChange = (event) => {
    setSelectedState(event.target.value);
  };
  const handleSelected = (event) => {
    console.log(event.target.value)
    setSelectedCity(event.target.value);
  }

  return (
    <div className="container">
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            width: "30px",
            height: "40px",
          }}
        >
          <img src={warehouse} alt="" />
        </div>
        <div style={{ width: "80%" }}>
          <input
            type="text"
            placeholder="Search by Name, State, or City"
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
          />
        </div>

        <div>
          <button
            type="button"
            class="btn btn-primary"
            data-toggle="modal"
            data-target="#exampleModal"
            onClick={handleAdd}
            style={{
              color: "white",
              backgroundColor: "purple",
              padding: "10px",
              borderRadius:'30px',
              width:'130px'
            }}
          >
            Add New
          </button>
        </div>
      </div>

      <table className="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Status</th>
            <th>State</th>
            <th>City</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {filteredData.map(warehouse => (
            <tr key={warehouse.id}>
              <td>{warehouse.name}</td>
              <td className={warehouse.status === 'Active' ? 'status-active' : 'status-inactive'}>
                {warehouse.status}
              </td>
              <td>{warehouse.state}</td>
              <td>{warehouse.city}</td>
              <td style={{display:"flex", gap:"10px"}}>
                <button onClick={() => handleEdit(warehouse)}><FaEdit style={{color:"gray"}}/></button>
                <button onClick={() => handleDelete(warehouse)}><RiDeleteBin6Line style={{color:"gray"}}/></button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      {showAddModal && (
        <div className="modal-container show">
          <div className="modal">
            <span className="close" onClick={() => setShowAddModal(false)}>&times;</span>
            <h2>Add Warehouse</h2>
            <label>Name:</label>
            <input type="text" value={newWarehouse.name} onChange={(e) => setNewWarehouse({ ...newWarehouse, name: e.target.value })} />
            <label>Status:</label>
            <input type="text" value={newWarehouse.status} onChange={(e) => setNewWarehouse({ ...newWarehouse, status: e.target.value })} />
            <label>State</label>
            <input type="text" value={newWarehouse.state} onChange={(e) => setNewWarehouse({ ...newWarehouse, state: e.target.value })} />
            <label>City</label>
            <input type="text" value={newWarehouse.city} onChange={(e) => setNewWarehouse({ ...newWarehouse, city: e.target.value })} />
            <button onClick={handleAddSave}>Save</button>
          </div>
        </div>
      )}

      {showEditModal && (
        <div className="modal-container show">
          <div className="modal">
            <span className="close" onClick={() => setShowEditModal(false)}>&times;</span>
            <h2>Edit Warehouse</h2>
            <label>Name:</label>
            <input type="text" value={newWarehouseName} onChange={(e) => setNewWarehouseName(e.target.value)} />
            <label>Status:</label>
            <input type="text" value={newStatus} onChange={(e) => setNewStatus(e.target.value)} />

            <label>State</label>
            <input type="text" value={newState} onChange={(e) => setNewState(e.target.value)} />
            <label>City</label>
            <input type="text" value={newCity} onChange={(e) => setNewCity(e.target.value)} />
            <button onClick={handleEditSave}>Save Changes</button>
          </div>
        </div>
      )}

      {showDeleteModal && (
        <div className="modal-container show">
          <div className="modal">
            <span className="close" onClick={() => setShowDeleteModal(false)}>&times;</span>
            <h2>Confirm Delete</h2>
            <p>Are you sure you want to delete this warehouse?</p>
            <button onClick={handleDeleteConfirm}>Delete</button>
            <button onClick={() => setShowDeleteModal(false)}>Cancel</button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Warehouse;
