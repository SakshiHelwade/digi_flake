import React, { useContext, useState } from 'react';
import '../style/home.css';
import myImage from '../assets/images/image 4.png';
import { useNavigate } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';
import { FaRegUserCircle } from "react-icons/fa";

const Home = () => {
  const [showConfirmation, setShowConfirmation] = useState(false);
  const {logout} = useContext(AuthContext)
  const navigate = useNavigate()
  const handleLogout = () => {
    setShowConfirmation(true);
  };

  const confirmLogout = () => {
    logout();
    navigate('/')
  };

  const cancelLogout = () => {
    setShowConfirmation(false);
  };

  return (
    <div>
      {/* <div className="nav">
        <button onClick={handleLogout} >
          <FaRegUserCircle style={{color:"whitesmoke", width:"30px",height:"30px"}}/>
          </button>
      </div> */}
      <div className='imgs'>
        <img src={myImage} alt="" className='logo' />
        <p>Welcome to Digitalflake admin</p>
      </div>

      {/* {showConfirmation && (
        <div className="confirmation-dialog">
          <p>Log Out</p>
          <p>Are you sure you want to log out?</p>
          <button onClick={confirmLogout}>Yes</button>
          <button onClick={cancelLogout}>No</button>
        </div>
      )} */}
    </div>
  );
};

export default Home;
