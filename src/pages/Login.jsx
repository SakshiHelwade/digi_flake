import React, { useContext, useState } from 'react';
import '../style/Login.css';
import myImage from '../assets/images/image 4.png';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { AuthContext } from '../context/AuthContext';

// Modal component
const ForgetPasswordModal = ({ isOpen, onClose }) => {
    const [email, setEmail] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
    };

    return (
        <div className={`modal ${isOpen ? 'open' : ''}`}>
            <div className="modal-content">
                <span className="close" onClick={onClose}>&times;</span>
                <h2>Forgot Password</h2>
                <form onSubmit={handleSubmit}>
                    <input
                        type="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="Enter your email"
                        required
                    />
                    <button type="submit">Submit</button>
                </form>
            </div>
        </div>
    );
};

const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const [loading,setLoading] = useState(false);
    const [showForgetPasswordModal, setShowForgetPasswordModal] = useState(false);
    const {login} = useContext(AuthContext)

    const navigate = useNavigate();

    const handleLogin = async (e) => {
        e.preventDefault();
        setLoading(true);
        try {
            const response = await axios.post(`https://reqres.in/api/login`, {
                email: email,
                password: password,
            });
            setLoading(false);
            if (response) {
                console.log(response.data);
                login(response.data.token)
                navigate("/home");
            }
        } catch (error) {
            setLoading(false);
            console.log(error.response?.data.msg);
            setError(error.response?.data.msg);
        }
    };

    const handleForgetPassword = () => {
        setShowForgetPasswordModal(true); // Open the modal
    };

    const handleCloseForgetPasswordModal = () => {
        setShowForgetPasswordModal(false);
    };


    return (
        <div className='Container'>
            <div className='formContaier'>
                <form onSubmit={handleLogin} >
                    <img src={myImage} alt="My Image" className='logo' />
                    <h1>Welcome to Digitalflake admin</h1>
                    <input
                        type="text"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="Email"
                    />
                    <input
                        type="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        placeholder="Password"
                    />
                    <button
                        type="submit"
                        className="bg-blue-500 text-white font-semibold py-2 px-4 rounded hover:bg-blue-600"
                    >
                        { loading ? "Signing in..." : "Login"}
                    </button>
                    {error && <p>{error}</p>}
                    <p onClick={handleForgetPassword}>Forget password ?</p>
                </form>
            </div>
            {showForgetPasswordModal && <ForgetPasswordModal
                isOpen={showForgetPasswordModal}
                onClose={handleCloseForgetPasswordModal}
            />}


        </div>
    );
};

export default Login;
