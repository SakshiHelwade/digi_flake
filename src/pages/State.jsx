import React, { useEffect, useState } from 'react';
import "../style/State.css";
import { FaEdit } from "react-icons/fa";
import { RiDeleteBin6Line } from "react-icons/ri";
import state from "../assets/images/map.png";

const State = () => {
  const [data, setData] = useState([]);
  const [showEditModal, setShowEditModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [showAddModal, setShowAddModal] = useState(false);
  const [selectedState, setSelectedState] = useState(null);
  const [newStateName, setNewStateName] = useState('');
  const [newStateCode, setNewStateCode] = useState('');
  const [newStatus, setNewStatus] = useState('');
  const [newState, setNewState] = useState({
    StateName: '',
    StateCode: '',
    Status: ''
  });
  const [searchTerm, setSearchTerm] = useState('');


  const handleEdit = (state) => {
    setSelectedState(state);
    setNewStateName(state.StateName);
    setNewStateCode(state.StateCode);
    setNewStatus(state.Status);
    setShowEditModal(true);
  };

  const handleDelete = (state) => {
    
    if(state.Status === "Active"){
      const newData = {
        StateName: state.StateName,
        StateCode: state.StateCode,
        Status: "Inactive"
      };
      const updatedData = data.map(item =>
        item.id === state.id ? { ...item, ...newData } : item
      );
      setData(updatedData);
    }else if(state.Status === 'Inactive'){
      setSelectedState(state);
      setShowDeleteModal(true);
    }
  };

  const handleAdd = () => {
    setShowAddModal(true);
  };

  const handleAddSave = () => {
    const payload = {
      id: Date.now(),
      ...newState
    };
    setData([...data, payload]);
    setShowAddModal(false);
    setNewState({ StateName: '', StateCode: '', Status: '' });
  };

  const handleEditSave = () => {
    const newData = {
      StateName: newStateName,
      StateCode: newStateCode,
      Status: newStatus
    };
    const updatedData = data.map(item =>
      item.id === selectedState.id ? { ...item, ...newData } : item
    );
    setData(updatedData);
    setShowEditModal(false);
    setSelectedState(null);
  };

  const handleDeleteConfirm = () => {
    const updatedData = data.filter(item => item.id !== selectedState.id);
    setData(updatedData);
    setShowDeleteModal(false);
    setSelectedState(null);
  };

  // Filter data based on search term
  const filteredData = data.filter(state =>
    state.StateName.toLowerCase().includes(searchTerm.toLowerCase()) ||
    state.StateCode.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <div className="container">
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            width: "30px",
            height: "40px",
          }}
        >
          <img src={state} alt="" />
        </div>
        <div style={{ width: "80%" }}>
          <input
            type="text"
            placeholder="Search by Name, State, or City"
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
          />
        </div>

        <div>
          <button
            type="button"
            class="btn btn-primary"
            data-toggle="modal"
            data-target="#exampleModal"
            onClick={handleAdd}
            style={{
              color: "white",
              backgroundColor: "purple",
              padding: "10px",
              borderRadius:'30px',
              width:'130px'
            }}
          >
            Add New
          </button>
        </div>
      </div>

      <table className="table">
        <thead>
          <tr>
            <th>State Name</th>
            <th>State Code</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {filteredData.map(state => (
            <tr key={state.id}>
              <td>{state.StateName}</td>
              <td>{state.StateCode}</td>
              <td className={state.Status === 'Active' ? 'status-active' : 'status-inactive'}>{state.Status}</td>
              <td style={{display:"flex", gap:"10px"}}>
                <button onClick={() => handleEdit(state)}><FaEdit style={{color:"gray"}}/></button>
                <button onClick={() => handleDelete(state)}><RiDeleteBin6Line style={{color:"gray"}}/></button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      {showAddModal && (
        <div className="modal-container show">
          <div className="modal">
            <span className="close" onClick={() => setShowAddModal(false)}>&times;</span>
            <h2>Add State</h2>
            <label>State Name:</label>
            <input type="text" value={newState.StateName} onChange={(e) => setNewState({ ...newState, StateName: e.target.value })} />
            <label>State Code:</label>
            <input type="text" value={newState.StateCode} onChange={(e) => setNewState({ ...newState, StateCode: e.target.value })} />
            <label>Status:</label>
            <input type="text" value={newState.Status} onChange={(e) => setNewState({ ...newState, Status: e.target.value })} />
            <button onClick={handleAddSave}>Save</button>
          </div>
        </div>
      )}

      {showEditModal && (
        <div className="modal-container show">
          <div className="modal">
            <span className="close" onClick={() => setShowEditModal(false)}>&times;</span>
            <h2>Edit State</h2>
            <label>State Name:</label>
            <input type="text" value={newStateName} onChange={(e) => setNewStateName(e.target.value)} />
            <label>State Code:</label>
            <input type="text" value={newStateCode} onChange={(e) => setNewStateCode(e.target.value)} />
            <label>Status:</label>
            <input type="text" value={newStatus} onChange={(e) => setNewStatus(e.target.value)} />
            <button onClick={handleEditSave}>Save Changes</button>
          </div>
        </div>
      )}

      {showDeleteModal && (
        <div className="modal-container show">
          <div className="modal">
            <span className="close" onClick={() => setShowDeleteModal(false)}>&times;</span>
            <h2>Confirm Delete</h2>
            <p>Are you sure you want to delete this state?</p>
            <button onClick={handleDeleteConfirm}>Delete</button>
            <button onClick={() => setShowDeleteModal(false)}>Cancel</button>
          </div>
        </div>
      )}
    </div>
  );
};

export default State;
