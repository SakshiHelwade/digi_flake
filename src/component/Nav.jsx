import React, { useContext, useState } from 'react';
import "../style/home.css";
import { AuthContext } from '../context/AuthContext';
import { FaRegUserCircle } from "react-icons/fa";
import { useLocation, useNavigate } from 'react-router-dom';
import logo from "../assets/images/Group 2609047 (1) 3.png"


const Nav = () => {
  const [showConfirmation, setShowConfirmation] = useState(false);
  const {logout} = useContext(AuthContext)
  const navigate = useNavigate()
  const location = useLocation();
  const isLoginPage = location.pathname === '/';

  const handleLogout = () => {
    setShowConfirmation(true);
  };

  const confirmLogout = () => {
    logout();
    setShowConfirmation(false);
    navigate('/')
  };

  const cancelLogout = () => {
    setShowConfirmation(false);
  };

  return (
    <>
    <div className={`${isLoginPage ? 'hidden' : 'nav'}`}>
      <div>
        <img src={logo} alt="" />
      </div>
        <button onClick={handleLogout} >
          <FaRegUserCircle style={{color:"whitesmoke", width:"30px",height:"30px"}}/>
          </button>
      </div>
      {showConfirmation && (
        <div className="confirmation-dialog">
          <p>Log Out</p>
          <p>Are you sure you want to log out?</p>
          <button onClick={confirmLogout}>Yes</button>
          <button onClick={cancelLogout}>No</button>
        </div>
      )}
      </>
  );
};

export default Nav;
